package de.metager.metagerapp.tokens;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.util.ArrayMap;
import android.webkit.CookieManager;

import androidx.annotation.NonNull;

import com.google.common.util.concurrent.AsyncCallable;
import com.google.common.util.concurrent.AsyncFunction;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.metager.metagerapp.metager.Preferences;

/**
 * Class to manage Tokens in the App which are used to provide
 * an anonymous way to use a MetaGer Key anonymously.
 */
public class TokenManager {
    private static final TokenManager instance = new TokenManager();

    private final int cookieMaxAge = 60 * 60 * 24 * 365 * 5; // Store Cookies for five years
    private String key;
    private final static String RFC1123_PATTERN = "EEE, dd MMM yyyy HH:mm:ss z";
    public String apiBase;
    public String baseURL;

    private ExecutorService workers;

    public TokenBag tokens;
    private float keycharge;

    private ListenableFuture<Void> asyncRefill = null;

    private TokenManager() {

    }

    public static synchronized TokenManager getInstance() {
        return instance;
    }

    public void init(String base_url) {

        workers = Executors.newCachedThreadPool();

        baseURL = base_url;
        apiBase = base_url + "/keys/api/json";

        SharedPreferences prefs = Preferences.getDefault();
        String first_use_key = "tokenmanager_first_usage";
        long first_usage;
        if (prefs.contains(first_use_key)) {
            first_usage = prefs.getLong("tokenmanager_first_usage", 0);
        } else {
            first_usage = System.currentTimeMillis();
            prefs.edit().putLong(first_use_key, first_usage).apply();
        }

        try {
            this.keycharge = prefs.getFloat("tokenmanager_keycharge", 0);
        } catch (ClassCastException e) {
            // The app probably has an old value stored as integer which needs to get converted
            // Can be removed in the Future
            this.keycharge = prefs.getInt("tokenmanager_keycharge", 0);
        }
        tokens = new TokenBag(new ArrayList<Token>(), new ArrayList<Token>());
        loadKey();
        loadTokens();
    }

    /**
     * Searches Browserdata for a given URL if it contains a key or leftover Tokens to be recycled
     *
     * @param url
     */
    public void consumeBrowserdata(URL url) {
        CookieManager cookieManager = CookieManager.getInstance();
        String cookies_string = cookieManager.getCookie(url.toString());
        if (cookies_string == null) return;

        Date expiration = new Date(0);
        DateFormat format = new SimpleDateFormat(TokenManager.RFC1123_PATTERN, Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("GMT"));

        String newCookie = "";
        for (String cookie_string : cookies_string.split(";")) {
            cookie_string = cookie_string.trim();
            String[] cookie_split = cookie_string.split("=");
            if (cookie_split.length >= 2) {
                String key = cookie_split[0].trim();
                String value = null;
                try {
                    value = URLDecoder.decode(cookie_split[1].trim(), "utf-8");
                } catch (UnsupportedEncodingException e) {
                    continue;
                }
                boolean tokenAdded = false;
                switch (key) {
                    case "key":
                        this.storeKey(value);
                        String cookie_value_string = "key=deleted; path=/; Expires=" + format.format(expiration);
                        cookieManager.setCookie(url.getProtocol() + "://" + url.getHost(), cookie_value_string);
                        break;
                    case "tokens":
                        try {
                            JsonArray tokens = (new Gson()).fromJson(value, JsonArray.class);
                            for (JsonElement token_element : tokens) {
                                JsonObject token_object = token_element.getAsJsonObject();
                                Token newToken = Token.fromJSON(token_object);
                                if (newToken != null) {
                                    this.tokens.unshiftToken(newToken);
                                    tokenAdded = true;
                                }
                            }
                        } catch (JsonSyntaxException | IllegalStateException ignored) {
                        }
                        cookieManager.setCookie(url.toString(), "tokens=deleted; path=/; Expires=" + format.format(expiration));
                        cookieManager.flush();
                        break;
                    case "decitokens":
                        try {
                            JsonArray tokens = (new Gson()).fromJson(value, JsonArray.class);
                            for (JsonElement token_element : tokens) {
                                JsonObject token_object = token_element.getAsJsonObject();
                                Token newToken = Token.fromJSON(token_object);
                                if (newToken != null) {
                                    this.tokens.unshiftDecitoken(newToken);
                                    tokenAdded = true;
                                }
                            }
                        } catch (JsonSyntaxException | IllegalStateException ignored) {
                        }
                        cookieManager.setCookie(url.toString(), "decitokens=deleted; path=/; Expires=" + format.format(expiration));
                        cookieManager.flush();
                        break;
                }
                if (tokenAdded) {
                    this.saveTokens();
                }
            }
        }
    }

    public Map<String, String> getHeaders(URL url) {
        Map<String, String> headers = new ArrayMap<>();
        if (this.key != null) {
            if (this.isCookieManagementPath(url)) {
                headers.put("key", this.key);
            } else {
                // Some headers will always be added
                headers.put("tokensource", "app");

                if (keycharge == 0 && key != null) {
                    try {
                        this.fetchKeyCharge().get();
                    } catch (ExecutionException | InterruptedException ignored) {
                    }
                }

                String tokenauthorization = "empty";
                if (keycharge > 0 || tokens.availableTokenCount() > 0) {
                    tokenauthorization = "low";
                }
                if (keycharge > 30) {
                    tokenauthorization = "full";
                }
                headers.put("tokenauthorization", tokenauthorization);
                if (this.isTokenPath(url)) {
                    headers.put("anonymous-token-payment-id", UUID.randomUUID().toString());
                }
            }
        }
        return headers;
    }

    public void loadTokens() {
        SharedPreferences prefs = Preferences.getDefault();

        this.tokens.clear();

        String tokensString = prefs.getString("tokens", null);
        if (tokensString == null) {
            return;
        }

        try {
            JsonObject tokensJson = JsonParser.parseString(tokensString).getAsJsonObject();
            for (JsonElement tokenElement : tokensJson.get("tokens").getAsJsonArray()) {
                JsonObject tokenObject = tokenElement.getAsJsonObject();
                this.tokens.addToken(Token.fromJSON(tokenObject));
            }
            for (JsonElement tokenElement : tokensJson.get("decitokens").getAsJsonArray()) {
                JsonObject tokenObject = tokenElement.getAsJsonObject();
                this.tokens.addDecioken(Token.fromJSON(tokenObject));
            }
        } catch (IllegalStateException e) {
            // Version 5.1.7 only stored Tokens directly as Array. If we are loading an old state we can handle it, too
            JsonArray tokensJson = JsonParser.parseString(tokensString).getAsJsonArray();
            for (JsonElement token : tokensJson) {
                JsonObject tokenJson = token.getAsJsonObject();
                Token loadedToken = Token.fromJSON(tokenJson);
                if (loadedToken != null) {
                    tokens.addToken(loadedToken);
                }
            }
        }
    }

    public void saveTokens() {
        SharedPreferences prefs = Preferences.getDefault();
        prefs.edit().putString("tokens", tokens.toString()).apply();
    }

    public String getKey() {
        return this.key;
    }

    public boolean isTokenPath(URL url) {
        return url.getPath().matches(".*/meta/meta.ger3$");
    }

    public boolean isCookieManagementPath(URL url) {
        if (url.getPath().matches(".*/meta/key$")) {
            // Deprecated MetaGer Key page; Will be removed soon
            return true;
        } else // Key Management Pages
            if (url.getPath().matches(".*/meta/settings$")) {
                // MetaGer Settings page
                return true;
            } else return url.getPath().matches(".*/keys/key/.*$");
    }

    private void storeKey(String key) {
        this.key = key;
        SharedPreferences prefs = Preferences.getDefault();
        prefs.edit().putString("key", key).apply();
    }

    public void removeKey() {
        this.key = null;
        this.keycharge = 0;
        Preferences.getDefault().edit().remove("key").apply();
    }

    private void loadKey() {
        SharedPreferences prefs = Preferences.getDefault();
        key = prefs.getString("key", null);
    }

    public ListenableFuture<Float> fetchKeyCharge() {
        ListenableFuture<Float> keychargeFetcherTask = Futures.submit(new FetchKeyCharge(this.key), this.workers);
        Futures.addCallback(keychargeFetcherTask, new FutureCallback<Float>() {
            @Override
            public void onSuccess(Float result) {
                TokenManager.this.setKeyCharge(result);
            }

            @Override
            public void onFailure(@NonNull Throwable t) {

            }
        }, workers);
        return keychargeFetcherTask;
    }

    public void syncPayment(String payment_id) {
        // Retrieves cost calculation from MetaGer Server
        ListenableFuture<PaymentInformation> fetchPaymentTask = this.fetchPayment(payment_id);
        // Checks if there are enough anonymous Tokens to make this payment and refills otherwise
        ListenableFuture<PaymentInformation> refillTokenTask = Futures.transformAsync(fetchPaymentTask, new AsyncFunction<PaymentInformation, PaymentInformation>() {
            @SuppressLint("CheckResult")
            @NonNull
            @Override
            public ListenableFuture<PaymentInformation> apply(PaymentInformation input) throws Exception {
                // Queue an async refill but only if there is no refill queued yet
                if (asyncRefill == null || asyncRefill.isDone()) {
                    asyncRefill = Futures.submit(() -> {
                        double waitTime = Math.random() * 30000;
                        try {
                            Thread.sleep((long) waitTime);
                            Futures.submit(new TokenRefill(input), workers);
                        } catch (InterruptedException ignored) {
                        }
                    }, workers);
                }
                return Futures.submit(new TokenRefill(input), workers);
            }
        }, this.workers);
        // Submits the required Token amount to execute the payment
        ListenableFuture<PaymentInformation> makePaymentTask = Futures.transformAsync(refillTokenTask, new AsyncFunction<PaymentInformation, PaymentInformation>() {
            @NonNull
            @Override
            public ListenableFuture<PaymentInformation> apply(PaymentInformation input) throws Exception {
                return Futures.submit(new MakePayment(input), workers);
            }
        }, workers);
        ListenableFuture<PaymentInformation> refillTask = Futures.transformAsync(makePaymentTask, new AsyncFunction<PaymentInformation, PaymentInformation>() {
            @NonNull
            @Override
            public ListenableFuture<PaymentInformation> apply(PaymentInformation input) throws Exception {
                double waitTime = (Math.random() * 25000) + 5000;
                Thread.sleep((long) waitTime);
                return Futures.submit(new TokenRefill(input), workers);
            }
        }, workers);

    }

    public ListenableFuture<PaymentInformation> fetchPayment(String payment_id) {
        ListenableFuture<PaymentInformation> fetchPaymentTask = Futures.submit(new FetchPayment(payment_id), this.workers);
        return fetchPaymentTask;
    }

    private Map<String, String> parseCookies(String cookieDomain) {
        CookieManager cookieManager = CookieManager.getInstance();

        Map<String, String> cookies = new HashMap<>();

        String cookieString = cookieManager.getCookie(cookieDomain);
        if (cookieString == null) {
            // No Cookie defined
            return cookies;
        }
        String[] cookieArray;
        if (cookieString.contains(";")) {
            // Multiple Cookies defined
            cookieArray = cookieString.split(";");
        } else {
            // Just a single cookie is defined
            cookieArray = new String[1];
            cookieArray[0] = cookieString;
        }

        Pattern cookiePattern = Pattern.compile("^([^=]+)=(.*)");

        for (String singleCookie : cookieArray) {
            singleCookie = singleCookie.trim();
            Matcher cookie_matcher = cookiePattern.matcher(singleCookie);
            if (cookie_matcher.matches()) {
                cookies.put(cookie_matcher.group(1), cookie_matcher.group(2));
            }
        }
        return cookies;
    }

    public float getKeycharge() {
        return this.keycharge;
    }

    public void setKeyCharge(float keycharge) {
        this.keycharge = keycharge;
        SharedPreferences prefs = Preferences.getDefault();
        prefs.edit().putFloat("tokenmanager_keycharge", keycharge).apply();
    }


}
