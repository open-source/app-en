package de.metager.metagerapp.tokens;

import com.google.gson.JsonObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Callable;

import de.metager.metagerapp.http.Agent;

public class FetchKeyCharge implements Callable<Float> {
    private final String key;

    public FetchKeyCharge(String key) {
        this.key = key;
    }

    @Override
    public Float call() throws Exception {
        URL api_url = new URL(TokenManager.getInstance().apiBase + "/key/" + key);
        HttpURLConnection con = (HttpURLConnection) api_url.openConnection();
        con.setRequestProperty("User-Agent", Agent.USERAGENT());
        con.setRequestProperty("Accept", "application/json");
        if (con.getResponseCode() == 200) {
            JsonObject response = Agent.PARSE_HTTP_RESPONSE(con);
            return response.get("charge").getAsFloat();
        } else {
            throw new Exception("Key API Server returned response code \"" + con.getResponseCode() + "\"");
        }
    }
}
