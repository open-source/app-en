package de.metager.metagerapp.tokens;

import androidx.annotation.NonNull;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;

public class PaymentInformation {
    public final float cost;
    public final String paymentUID;
    public final String paymentID;
    public float missing;
    public String key;
    public final float credits;
    public final boolean async_disabled;
    public final TokenBag tokens;

    public PaymentInformation(float cost, float missing, String payment_id, String payment_uid, String key, float credits, boolean async_disabled, TokenBag tokens) {
        this.cost = cost;
        this.missing = missing;
        this.paymentID = payment_id;
        this.paymentUID = payment_uid;
        this.key = key;
        this.credits = credits;
        this.async_disabled = async_disabled;
        this.tokens = tokens;
    }

    public static PaymentInformation FROM_JSON(JsonObject data) {
        float cost = data.get("cost").getAsFloat();
        float missing = data.get("missing").getAsFloat();
        String payment_id = null;
        try {
            payment_id = data.get("payment_id").getAsString();
        } catch (UnsupportedOperationException | NullPointerException ignored) {
        }
        String payment_uid = null;
        try {
            payment_uid = data.get("payment_uid").getAsString();
        } catch (UnsupportedOperationException | NullPointerException ignored) {
        }
        String key = null;
        try {
            key = data.get("key").getAsString();
        } catch (UnsupportedOperationException | NullPointerException ignored) {
        }
        float credits = data.get("credits").getAsFloat();
        boolean async_disabled = data.get("async_disabled").getAsBoolean();
        JsonObject tokens = data.get("tokens").getAsJsonObject();
        ArrayList<Token> token_array = new ArrayList<>();
        for (JsonElement token_element : tokens.get("tokens").getAsJsonArray()) {
            JsonObject token = token_element.getAsJsonObject();
            token_array.add(Token.fromJSON(token));
        }

        ArrayList<Token> decitoken_array = new ArrayList<>();
        for (JsonElement token_element : tokens.get("decitokens").getAsJsonArray()) {
            JsonObject token = token_element.getAsJsonObject();
            decitoken_array.add(Token.fromJSON(token));
        }
        return new PaymentInformation(cost, missing, payment_id, payment_uid, key, credits, async_disabled, new TokenBag(token_array, decitoken_array));
    }

    public JsonObject toJson() {
        JsonObject output = new JsonObject();
        output.addProperty("cost", cost);
        output.addProperty("missing", missing);
        output.addProperty("payment_id", paymentID);
        output.addProperty("payment_uid", paymentUID);
        output.addProperty("key", key);
        output.addProperty("credits", credits);
        output.addProperty("async_disabled", async_disabled);
        output.add("tokens", tokens.toJSON());
        return output;
    }

    @NonNull
    public String toString() {
        return this.toJson().toString();
    }
}
