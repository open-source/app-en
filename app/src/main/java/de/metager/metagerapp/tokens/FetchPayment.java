package de.metager.metagerapp.tokens;

import com.google.gson.JsonObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.concurrent.Callable;

import de.metager.metagerapp.http.Agent;

public class FetchPayment implements Callable<PaymentInformation> {

    private final String paymentID;

    public FetchPayment(String payment_id) {
        this.paymentID = payment_id;
    }

    @Override
    public PaymentInformation call() throws Exception {
        URL api_url = new URL(TokenManager.getInstance().baseURL + "/anonymous-token/cost?anonymous_token_payment_id=" + URLEncoder.encode(this.paymentID, "UTF-8"));
        HttpURLConnection con = (HttpURLConnection) api_url.openConnection();
        con.setRequestProperty("User-Agent", Agent.USERAGENT());
        con.setRequestProperty("Accept", "application/json");
        if (con.getResponseCode() == 200) {
            JsonObject response = Agent.PARSE_HTTP_RESPONSE(con);
            return PaymentInformation.FROM_JSON(response);
        } else {
            throw new Exception("Key API Server returned response code \"" + con.getResponseCode() + "\"");
        }
    }
}
