package de.metager.metagerapp.tokens;

import androidx.annotation.NonNull;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.UUID;

public class Token {
    public final UUID token;
    private final Pubkey pubkey;
    public BigInteger signature;
    public final String date;

    // Random Number that is used to Blind our token
    // r is relatively prime to the used pubkey
    private final BigInteger r;

    public Token(Pubkey pubkey) {
        this.token = UUID.randomUUID();
        this.pubkey = pubkey;
        this.r = calculateR();
        this.date = pubkey.date;
    }


    private Token(String token, String signature, String date){
        this.token = UUID.fromString(token);
        this.signature = new BigInteger(signature);
        this.date = date;
        this.r = null;
        this.pubkey = null;
    }



    /**
     * Generates a random Number using a random Number generator
     * Because of the nature of Blind signatures r must be picked such that
     * it is relatively prime to N of the MetaGer Servers pubkey
     *
     * r is used to compute a blinding factor r^e mod N
     * @return Random Number matching described criteria
     */
    private BigInteger calculateR() {
        BigInteger N = pubkey.pubkey.getModulus();

        // Random Number Generator
        SecureRandom random;
        try {
            random = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException(ex);
        }
        byte[] randomBytes = new byte[10];
        BigInteger one = new BigInteger("1");
        BigInteger gcd;

        BigInteger r; // Resulting r
        do {
            random.nextBytes(randomBytes);
            r = new BigInteger(randomBytes);
            gcd = r.gcd(N);
        }while(!gcd.equals(one) || r.compareTo(N) >= 0 || r.compareTo(one) <= 0);
        return r;
    }

    public BigInteger getBlindedToken(){
        BigInteger tokenBigInt = getToken();
        return ((r.modPow(pubkey.pubkey.getPublicExponent(), pubkey.pubkey.getModulus())).multiply(tokenBigInt)).mod(pubkey.pubkey.getModulus());
    }

    public void addSignature(String blindedSignature) {
        BigInteger N = pubkey.pubkey.getModulus();
        BigInteger E = pubkey.pubkey.getPublicExponent();
        BigInteger signature = new BigInteger(blindedSignature);
        signature = signature.multiply(r.modInverse(N)).mod(N);

        // Verify the token If the signature is correct then signature.modPow(E,N) should equal our token
        BigInteger signatureVerification = signature.modPow(E, N);
        BigInteger tokenHash = getToken();
        if(signatureVerification.equals(tokenHash)){
            this.signature = signature;
        }
    }

    public static Token fromString(String tokenString) {
        if(tokenString == null){
            return null;
        }

        try {
            JSONObject tokenJSON = new JSONObject(tokenString);
            return new Token(
                    tokenJSON.getString("token"),
                    tokenJSON.getString("signature"),
                    tokenJSON.getString("date")
            );
        }catch(JSONException e){
            e.printStackTrace();
            return null;
        }
    }

    public static Token fromJSON(JsonObject tokenJson) {
        if(!tokenJson.has("token") || !tokenJson.has("signature") || ! tokenJson.has("date")){
            return null;
        }
        return new Token(
                tokenJson.get("token").getAsString(),
                tokenJson.get("signature").getAsString(),
                tokenJson.get("date").getAsString()
        );
    }

    @NonNull
    public String toString() {
        return toJSON().toString();
    }

    public JsonObject toJSON() {
        JsonObject tokenJson = new JsonObject();
        tokenJson.addProperty("token", token.toString());
        tokenJson.addProperty("signature", signature.toString());
        tokenJson.addProperty("date", date);
        return tokenJson;
    }

    public BigInteger getToken(){
        // Created Hashed version of the token
        byte[] message;
        try {
            message = MessageDigest.getInstance("SHA-256").digest(token.toString().getBytes(StandardCharsets.UTF_8));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        return new BigInteger(1, message);
    }
}
