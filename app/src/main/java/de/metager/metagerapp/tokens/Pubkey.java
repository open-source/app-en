package de.metager.metagerapp.tokens;

import java.security.interfaces.RSAPublicKey;

/**
 * MetaGer Key uses different private Keys
 * depending on the date of the tokens
 * In general there is a different pubkey for every month
 */
public class Pubkey {

    public final RSAPublicKey pubkey;
    public final String date;

    public Pubkey(RSAPublicKey pubkey, String date){
        this.pubkey = pubkey;
        this.date = date;
    }
}
