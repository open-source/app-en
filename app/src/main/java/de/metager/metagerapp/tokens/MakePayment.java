package de.metager.metagerapp.tokens;

import android.util.Log;

import com.google.gson.JsonObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.concurrent.Callable;

import de.metager.metagerapp.http.Agent;

public class MakePayment implements Callable<PaymentInformation> {

    private final PaymentInformation payment;

    public MakePayment(PaymentInformation payment) {
        this.payment = payment;
    }

    @Override
    public PaymentInformation call() throws Exception {
        if (payment.missing == 0) return null;
        if (TokenManager.getInstance().tokens.availableTokenCount() < payment.missing) {
            // Under normal circumstances tokens should be refilled at this point
            // If there still aren't enough token we'll try to authenticate by using a key
            return this.makeKeyPayment();
        } else {
            // Make a payment using existing anonymous token
            return this.makeTokenPayment();
        }
    }

    private PaymentInformation makeKeyPayment() throws IOException {
        if (TokenManager.getInstance().getKey() == null) return payment;
        payment.key = TokenManager.getInstance().getKey();
        Log.d("token_payment", "Make the following payment with " + TokenManager.getInstance().tokens.tokens.size() + " Tokens and " + TokenManager.getInstance().tokens.decitokens.size() + " Decitokens left using the key:");
        Log.d("token_payment", payment.toString());

        URL api_url = new URL(TokenManager.getInstance().baseURL + "/anonymous-token");
        HttpURLConnection con = (HttpURLConnection) api_url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", Agent.USERAGENT());
        con.setRequestProperty("Accept", "application/json");
        con.setRequestProperty("Content-Type", "application/json");
        con.setDoOutput(true);

        try (DataOutputStream os = new DataOutputStream(con.getOutputStream())) {
            os.writeBytes(payment.toString());
            os.flush();
        }
        int responseCode = con.getResponseCode();
        JsonObject response = Agent.PARSE_HTTP_RESPONSE(con);
        try {
            return PaymentInformation.FROM_JSON(response);
        } catch (Exception ignored) {
            return null;
        }
    }

    private PaymentInformation makeTokenPayment() throws IOException {
        payment.key = null;
        // Use up existing decitokens if they are to be depleted
        if (!TokenManager.getInstance().tokens.decitokens_in_use && TokenManager.getInstance().tokens.decitokens.size() >= 10 && payment.missing >= 1) {
            for (int i = 0; i < 10; i++) {
                payment.tokens.decitokens.add(TokenManager.getInstance().tokens.decitokens.remove(0));
                payment.missing = BigDecimal.valueOf(payment.missing - 0.1F).setScale(1, RoundingMode.HALF_UP).floatValue();
            }
        }
        // Add as many tokens as needed
        while (!TokenManager.getInstance().tokens.tokens.isEmpty() && payment.missing >= 1) {
            payment.tokens.tokens.add(TokenManager.getInstance().tokens.tokens.remove(0));
            payment.missing = BigDecimal.valueOf(payment.missing - 1F).setScale(1, RoundingMode.HALF_UP).floatValue();
        }

        // Add as many decitokens as needed
        while (!TokenManager.getInstance().tokens.decitokens.isEmpty() && payment.missing > 0) {
            payment.tokens.decitokens.add(TokenManager.getInstance().tokens.decitokens.remove(0));
            payment.missing = BigDecimal.valueOf(payment.missing - 0.1F).setScale(1, RoundingMode.HALF_UP).floatValue();
        }

        // Add as many tokens as needed if something is left
        while (!TokenManager.getInstance().tokens.tokens.isEmpty() && payment.missing > 0) {
            payment.tokens.tokens.add(TokenManager.getInstance().tokens.tokens.remove(0));
            payment.missing = BigDecimal.valueOf(payment.missing - 1F).setScale(1, RoundingMode.HALF_UP).floatValue();
        }
        TokenManager.getInstance().saveTokens();

        Log.d("token_payment", "Make the following payment with " + TokenManager.getInstance().tokens.tokens.size() + " Tokens and " + TokenManager.getInstance().tokens.decitokens.size() + " Decitokens left:");
        Log.d("token_payment", payment.toString());

        URL api_url = new URL(TokenManager.getInstance().baseURL + "/anonymous-token");
        HttpURLConnection con = (HttpURLConnection) api_url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", Agent.USERAGENT());
        con.setRequestProperty("Accept", "application/json");
        con.setRequestProperty("Content-Type", "application/json");
        con.setDoOutput(true);

        try (DataOutputStream os = new DataOutputStream(con.getOutputStream())) {
            os.writeBytes(payment.toString());
            os.flush();
        }
        int responseCode = con.getResponseCode();
        JsonObject response = Agent.PARSE_HTTP_RESPONSE(con);
        try {
            PaymentInformation responsePayment = PaymentInformation.FROM_JSON(response);
            for (Token token : responsePayment.tokens.tokens) {
                TokenManager.getInstance().tokens.tokens.add(0, token);
            }
            for (Token token : responsePayment.tokens.decitokens) {
                TokenManager.getInstance().tokens.decitokens.add(0, token);
            }
            TokenManager.getInstance().saveTokens();
            if (!responsePayment.tokens.tokens.isEmpty() || !responsePayment.tokens.decitokens.isEmpty()) {
                Log.d("token_payment", "Recycle the following unused Tokens:");
                Log.d("token_payment", responsePayment.toString());
            }
            return responsePayment;
        } catch (Exception ignored) {
            return null;
        }
    }
}
