package de.metager.metagerapp.tokens;

import androidx.annotation.NonNull;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class TokenBag {
    public final ArrayList<Token> tokens;
    public final ArrayList<Token> decitokens;
    /**
     * Will be set to false by TokenRefill when decitokens are not used anymore but
     * there still are some stored.
     */
    public boolean decitokens_in_use = true;

    public TokenBag(ArrayList<Token> tokens, ArrayList<Token> decitokens) {
        this.tokens = tokens;
        this.decitokens = decitokens;
    }

    public void clear() {
        this.tokens.clear();
        this.decitokens.clear();
    }

    public float availableTokenCount() {
        return BigDecimal.valueOf(tokens.size() + ((float) this.decitokens.size() / 10)).setScale(1, RoundingMode.HALF_UP).floatValue();
    }

    public void addToken(Token newToken) {
        for (Token token : tokens) {
            if (token.getToken().equals(newToken.getToken())) return;
        }
        tokens.add(newToken);
    }

    public void unshiftToken(Token newToken) {
        for (Token token : tokens) {
            if (token.getToken().equals(newToken.getToken())) return;
        }
        tokens.add(0, newToken);
    }

    public void addDecioken(Token newToken) {
        for (Token token : decitokens) {
            if (token.getToken().equals(newToken.getToken())) return;
        }
        decitokens.add(newToken);
    }

    public void unshiftDecitoken(Token newToken) {
        for (Token token : decitokens) {
            if (token.getToken().equals(newToken.getToken())) return;
        }
        decitokens.add(0, newToken);
    }

    public JsonObject toJSON() {
        JsonObject token_json = new JsonObject();

        JsonArray tokens = new JsonArray();
        for (Token token : this.tokens) {
            tokens.add(token.toJSON());
        }
        token_json.add("tokens", tokens);

        JsonArray decitokens = new JsonArray();
        for (Token token : this.decitokens) {
            decitokens.add(token.toJSON());
        }
        token_json.add("decitokens", decitokens);

        return token_json;
    }

    @NonNull
    public String toString() {
        return this.toJSON().toString();
    }


}
