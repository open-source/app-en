package de.metager.metagerapp.tokens;

import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import de.metager.metagerapp.http.Agent;
import de.metager.metagerapp.metager.Preferences;


public class TokenRefill implements Callable<PaymentInformation> {
    public static int TOKEN_REFILL_TARGET_MINUTES = 60;

    public static final String STORAGEKEY_TOKEN_LAST_CHARGE = "tokenmanager_token_last_use";
    public static final String STORAGEKEY_TOKEN_TARGET_AMOUNT = "tokenmanager_token_target_amount";

    public static final String STORAGEKEY_DECITOKEN_LAST_CHARGE = "tokenmanager_decitoken_last_use";
    public static final String STORAGEKEY_DECITOKEN_TARGET_AMOUNT = "tokenmanager_decitoken_target_amount";

    private final PaymentInformation payment;
    private Pubkey pubkeyTokens = null;
    private Pubkey pubkeyDecitokens = null;
    private final int token_missing;
    private final int decitoken_missing;

    public TokenRefill(PaymentInformation payment) {
        this.payment = payment;
        token_missing = (int) Math.floor(payment.missing);
        decitoken_missing = (int) Math.ceil((payment.missing - token_missing) * 10);
    }

    @Override
    public PaymentInformation call() throws Exception {
        // No refill necessary if we have the required amount of token available
        if (TokenManager.getInstance().tokens.tokens.size() >= token_missing && TokenManager.getInstance().tokens.decitokens.size() >= decitoken_missing)
            return payment;
        if (pubkeyTokens == null || pubkeyDecitokens == null)
            this.getPublicKey();
        this.createNewTokens();
        return payment;
    }

    private void createNewTokens() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {

        ArrayList<Token> newTokens = new ArrayList<>();
        ArrayList<Token> newDecitokens = new ArrayList<>();

        SharedPreferences prefs = Preferences.getDefault();
        int token_target_amount = prefs.getInt(TokenRefill.STORAGEKEY_TOKEN_TARGET_AMOUNT, 0);
        Date token_last_refill = new Date(prefs.getLong(TokenRefill.STORAGEKEY_TOKEN_LAST_CHARGE, 0));
        if (TimeUnit.MINUTES.convert(new Date().getTime() - token_last_refill.getTime(), TimeUnit.MILLISECONDS) > TokenRefill.TOKEN_REFILL_TARGET_MINUTES * 4L) {
            // Reset target amount after too much time
            token_target_amount = 0;
        }


        int decitoken_target_amount = prefs.getInt(TokenRefill.STORAGEKEY_DECITOKEN_TARGET_AMOUNT, 0);
        Date decitoken_last_refill = new Date(prefs.getLong(TokenRefill.STORAGEKEY_DECITOKEN_LAST_CHARGE, 0));
        if (TimeUnit.MINUTES.convert(new Date().getTime() - decitoken_last_refill.getTime(), TimeUnit.MILLISECONDS) > TokenRefill.TOKEN_REFILL_TARGET_MINUTES * 4L) {
            // Reset target amount after too much time
            decitoken_target_amount = 0;
        }

        int token_charge_target = Math.max(token_missing, token_target_amount);
        int decitoken_charge_target = Math.max(decitoken_missing, decitoken_target_amount);

        if (decitoken_charge_target == 0 && !TokenManager.getInstance().tokens.decitokens.isEmpty()) {
            TokenManager.getInstance().tokens.decitokens_in_use = false;
            decitoken_charge_target = 10;
        } else {
            TokenManager.getInstance().tokens.decitokens_in_use = true;
        }

        if (pubkeyTokens == null || pubkeyDecitokens == null) {
            return;
        }
        Log.d("token_refill", "Token Target: " + token_charge_target + ", Decitoken Target: " + decitoken_charge_target);
        while (token_charge_target - TokenManager.getInstance().tokens.tokens.size() - newTokens.size() > 0) {
            newTokens.add(new Token(pubkeyTokens));
        }
        while (decitoken_charge_target - TokenManager.getInstance().tokens.decitokens.size() - newDecitokens.size() > 0) {
            newDecitokens.add(new Token(pubkeyDecitokens));
        }
        if (newTokens.isEmpty() && newDecitokens.isEmpty()) {
            return;
        }

        // We generated new Tokens. They all need to get signed by our server
        // To do so we send the blinded (encrypted) tokens to our server which signs them
        // We receive back the signed,blinded signatures and then can unblind the signature
        JsonObject signaturePayload = new JsonObject();
        signaturePayload.addProperty("key", TokenManager.getInstance().getKey());

        JsonObject blindedTokensObject = new JsonObject();
        blindedTokensObject.addProperty("date", pubkeyTokens.date);
        JsonArray blindedTokens = new JsonArray();
        for (Token token : newTokens) {
            blindedTokens.add(token.getBlindedToken().toString());
        }
        blindedTokensObject.add("tokens", blindedTokens);
        signaturePayload.add("blinded_tokens", blindedTokensObject);

        JsonObject blindedDecitokensObject = new JsonObject();
        blindedDecitokensObject.addProperty("date", pubkeyDecitokens.date);
        JsonArray blindedDecitokens = new JsonArray();
        for (Token token : newDecitokens) {
            blindedDecitokens.add(token.getBlindedToken().toString());
        }
        blindedDecitokensObject.add("tokens", blindedDecitokens);
        signaturePayload.add("blinded_decitokens", blindedDecitokensObject);

        URL signRequestURL = new URL(TokenManager.getInstance().apiBase + "/token/v2/sign");
        HttpURLConnection request = (HttpURLConnection) signRequestURL.openConnection();
        request.setRequestMethod("POST");
        request.setRequestProperty("User-Agent", Agent.USERAGENT());
        request.setRequestProperty("Content-Type", "application/json");
        request.setRequestProperty("Accept", "application/json");
        request.setDoOutput(true);
        try (DataOutputStream os = new DataOutputStream(request.getOutputStream())) {
            os.writeBytes(signaturePayload.toString());
            os.flush();
        }
        if (request.getResponseCode() == 422) {
            // Either the submitted tokens were invalid or the key is empty
            // The server might respond with a keycharge
            try {
                JsonObject jsonResponse = Agent.PARSE_HTTP_RESPONSE(request);
                TokenManager.getInstance().setKeyCharge(jsonResponse.get("charge").getAsFloat());
            } catch (Exception ignored) {
            }
            return;
        } else if (request.getResponseCode() != 201) {
            return;
        }

        JsonObject jsonResponse = Agent.PARSE_HTTP_RESPONSE(request);

        TokenManager.getInstance().setKeyCharge(jsonResponse.get("charge").getAsFloat());
        JsonObject signedTokensObject = jsonResponse.getAsJsonObject("signed_tokens");
        JsonObject signedTokens = signedTokensObject.getAsJsonObject("tokens");
        Set<Map.Entry<String, JsonElement>> signedTokensEntrySet = signedTokens.entrySet();
        for (Map.Entry<String, JsonElement> signedToken : signedTokensEntrySet) {
            String blindedToken = signedToken.getKey();
            String blindedSignature = signedToken.getValue().getAsString();
            for (Token token : newTokens) {
                if (token.getBlindedToken().toString().equals(blindedToken)) {
                    token.addSignature(blindedSignature);
                }
            }
        }

        JsonObject signedDeciokensObject = jsonResponse.getAsJsonObject("signed_decitokens");
        JsonObject signedDecitokens = signedDeciokensObject.getAsJsonObject("tokens");
        Set<Map.Entry<String, JsonElement>> signedDecitokensEntrySet = signedDecitokens.entrySet();
        for (Map.Entry<String, JsonElement> signedToken : signedDecitokensEntrySet) {
            String blindedToken = signedToken.getKey();
            String blindedSignature = signedToken.getValue().getAsString();
            for (Token token : newDecitokens) {
                if (token.getBlindedToken().toString().equals(blindedToken)) {
                    token.addSignature(blindedSignature);
                }
            }
        }

        for (Token token : newTokens) {
            TokenManager.getInstance().tokens.addToken(token);
        }
        for (Token token : newDecitokens) {
            TokenManager.getInstance().tokens.addDecioken(token);
        }
        TokenManager.getInstance().saveTokens();

        SharedPreferences.Editor editor = prefs.edit();
        if (!newTokens.isEmpty()) {
            editor.putLong(TokenRefill.STORAGEKEY_TOKEN_LAST_CHARGE, new Date().getTime());
            if (TimeUnit.MINUTES.convert(new Date().getTime() - token_last_refill.getTime(), TimeUnit.MILLISECONDS) < TokenRefill.TOKEN_REFILL_TARGET_MINUTES) {
                editor.putInt(TokenRefill.STORAGEKEY_TOKEN_TARGET_AMOUNT, token_target_amount + token_missing);
            } else {
                editor.putInt(TokenRefill.STORAGEKEY_TOKEN_TARGET_AMOUNT, Math.max(token_target_amount - token_missing, 0));
            }
        }
        if (!newDecitokens.isEmpty()) {
            editor.putLong(TokenRefill.STORAGEKEY_DECITOKEN_LAST_CHARGE, new Date().getTime());
            if (TimeUnit.MINUTES.convert(new Date().getTime() - decitoken_last_refill.getTime(), TimeUnit.MILLISECONDS) < TokenRefill.TOKEN_REFILL_TARGET_MINUTES) {
                editor.putInt(TokenRefill.STORAGEKEY_DECITOKEN_TARGET_AMOUNT, decitoken_target_amount + decitoken_missing);
            } else {
                editor.putInt(TokenRefill.STORAGEKEY_DECITOKEN_TARGET_AMOUNT, Math.max(decitoken_target_amount - decitoken_missing, 0));
            }
        }
        editor.apply();
    }

    private void getPublicKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        URL pubkeyURL = new URL(TokenManager.getInstance().apiBase + "/token/v2/pubkey");
        HttpURLConnection request = (HttpURLConnection) pubkeyURL.openConnection();
        request.setRequestProperty("Useragent", Agent.USERAGENT());
        request.setRequestProperty("Accept", "application/json");
        try {
            request.connect();
            if (request.getResponseCode() == 200) {
                JsonObject jsonResponse = Agent.PARSE_HTTP_RESPONSE(request);

                JsonObject tokenPubkeyJSON = jsonResponse.get("tokens").getAsJsonObject();
                String date = tokenPubkeyJSON.get("date").getAsString();
                String pubkeyPCKS8 = tokenPubkeyJSON.get("pubkey_pem").getAsString();
                pubkeyPCKS8 = pubkeyPCKS8.replace("-----BEGIN PUBLIC KEY-----", "")
                        .replace(System.lineSeparator(), "")
                        .replace("-----END PUBLIC KEY-----", "");
                byte[] encodedPubkey = Base64.decode(pubkeyPCKS8, Base64.DEFAULT);
                KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encodedPubkey);
                this.pubkeyTokens = new Pubkey((RSAPublicKey) keyFactory.generatePublic(keySpec), date);

                JsonObject decitokenPubkeyJSON = jsonResponse.get("decitokens").getAsJsonObject();
                date = decitokenPubkeyJSON.get("date").getAsString();
                pubkeyPCKS8 = decitokenPubkeyJSON.get("pubkey_pem").getAsString();
                pubkeyPCKS8 = pubkeyPCKS8.replace("-----BEGIN PUBLIC KEY-----", "")
                        .replace(System.lineSeparator(), "")
                        .replace("-----END PUBLIC KEY-----", "");
                encodedPubkey = Base64.decode(pubkeyPCKS8, Base64.DEFAULT);
                keyFactory = KeyFactory.getInstance("RSA");
                keySpec = new X509EncodedKeySpec(encodedPubkey);
                this.pubkeyDecitokens = new Pubkey((RSAPublicKey) keyFactory.generatePublic(keySpec), date);
            } else {
                throw new InvalidKeySpecException("Failed Request to Keyserver");
            }
        } finally {
            request.disconnect();
        }
    }
}
