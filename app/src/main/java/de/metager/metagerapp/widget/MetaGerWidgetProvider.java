package de.metager.metagerapp.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import de.metager.metagerapp.R;
import de.metager.metagerapp.metager.MainActivity;

public class MetaGerWidgetProvider extends AppWidgetProvider {
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        final int N = appWidgetIds.length;

        /**
         * We will just open the app on a click on any view of the widget
         */
        for(int i = 0; i<N; i++){
            int appWidgetId = appWidgetIds[i];

            Intent intent = new Intent(context, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_IMMUTABLE);

            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.metager_appwidget);
            views.setOnClickPendingIntent(R.id.widget_logo, pendingIntent);
            views.setOnClickPendingIntent(R.id.widget_searchbutton, pendingIntent);
            views.setOnClickPendingIntent(R.id.widget_textview, pendingIntent);

            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }
}
