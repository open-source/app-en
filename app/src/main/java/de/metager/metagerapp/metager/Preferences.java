package de.metager.metagerapp.metager;

import android.content.Context;
import android.content.SharedPreferences;

import de.metager.metagerapp.BuildConfig;

public class Preferences {
    private static Preferences instance = new Preferences();

    private SharedPreferences preferences;

    public void init(Context context) {
        preferences = context.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
    }

    public static synchronized Preferences getInstance() {
        return instance;
    }

    public static synchronized SharedPreferences getDefault() {
        return instance.preferences;
    }
}
