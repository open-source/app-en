package de.metager.metagerapp.metager;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.webkit.CookieManager;
import android.webkit.WebView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.view.WindowCompat;

import java.net.MalformedURLException;
import java.net.URL;

import de.metager.metagerapp.BuildConfig;
import de.metager.metagerapp.R;

/**
 * Handles Darkmode/Lightmode for the app
 * so it reflects the user settings
 */
public class ThemeSwitcher {
    public static void APPLY_STORED_STATE(MainActivity mActivity) {
        SharedPreferences preferences = mActivity.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
        int storedTheme = preferences.getInt("dark_mode", AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);

        Integer webviewTheme = ThemeSwitcher.getStoredState(mActivity);
        if (webviewTheme != null) {
            storedTheme = (int) webviewTheme;
        }

        int currentTheme = AppCompatDelegate.getDefaultNightMode();

        if (storedTheme != currentTheme) {
            AppCompatDelegate.setDefaultNightMode(storedTheme);
            preferences.edit().putInt("dark_mode", storedTheme).apply();


            // Update Navigationbar appearance manually as it does not change with defaultNightMode state
            boolean lightNavigationBars = true;
            switch (storedTheme) {
                case AppCompatDelegate.MODE_NIGHT_YES:
                    lightNavigationBars = false;
                    break;
                case AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM:
                    // Check the currently active setting
                    if (ThemeSwitcher.isSystemNightmodeEnabled(mActivity)) {
                        lightNavigationBars = false;
                    }
                    break;
            }
            WindowCompat.getInsetsController(mActivity.getWindow(), mActivity.getWindow().getDecorView()).setAppearanceLightNavigationBars(lightNavigationBars);
            WindowCompat.getInsetsController(mActivity.getWindow(), mActivity.getWindow().getDecorView()).setAppearanceLightStatusBars(lightNavigationBars);
            mActivity.onNightModeChanged();
        }


    }

    public static void APPLY_DEFAULT_STATE(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
        int storedTheme = preferences.getInt("dark_mode", AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
        int currentTheme = AppCompatDelegate.getDefaultNightMode();
        if (storedTheme != currentTheme) {
            AppCompatDelegate.setDefaultNightMode(storedTheme);
        }
    }

    private static boolean isSystemNightmodeEnabled(Context context) {
        int currentNightMode = context.getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
        return currentNightMode == Configuration.UI_MODE_NIGHT_YES;
    }

    private static Integer getStoredState(MainActivity mActivity) {
        CookieManager cookieManager = CookieManager.getInstance();
        WebView webview = mActivity.findViewById(R.id.webview);

        try {
            String url = (new URL(webview.getUrl())).toString();
        } catch (MalformedURLException ignored) {
            return null;
        }

        String cookies = cookieManager.getCookie(webview.getUrl());
        if (cookies != null) {
            for (String cookie : cookies.split(";")) {
                cookie = cookie.trim();
                String key = cookie.split("=")[0];
                String value = cookie.split("=")[1];
                if (key.equals("dark_mode")) {
                    int darkmode_value = 0; // Follow System default
                    try {
                        darkmode_value = Integer.parseInt(value);
                    } catch (NumberFormatException ignored) {
                    }
                    switch (darkmode_value) {
                        case 1:
                            return AppCompatDelegate.MODE_NIGHT_NO;
                        case 2:
                            return AppCompatDelegate.MODE_NIGHT_YES;
                    }
                }
            }
        }
        return AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM;
    }
}
