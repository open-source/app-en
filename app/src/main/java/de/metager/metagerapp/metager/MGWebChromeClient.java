package de.metager.metagerapp.metager;

import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class MGWebChromeClient extends WebChromeClient {


    private final ProgressBar progressBar;
    private final MainActivity activity;

    // Variables for enabling Filechooser in Webview
    // Used by key login page
    private final ActivityResultLauncher<String> mGetContent;
    private ValueCallback<Uri[]> filePathCallback;
    public static final int STORAGE_PERMISSION_CODE = 1000;


    public MGWebChromeClient(ProgressBar progressBar, MainActivity activity) {
        this.progressBar = progressBar;
        this.activity = activity;

        mGetContent = activity.registerForActivityResult(new ActivityResultContracts.GetContent(),
                uri -> {
                    if(filePathCallback != null){
                        filePathCallback.onReceiveValue(new Uri[]{uri});
                        filePathCallback = null;
                    }
                });
    }

    @Override
    public void onProgressChanged(WebView view, int progress){
        if(progress < 100 && progressBar.getVisibility() == ProgressBar.GONE){
            progressBar.setVisibility(ProgressBar.VISIBLE);
        }

        progressBar.setProgress(progress);
        if(progress == 100) {
            progressBar.setVisibility(ProgressBar.GONE);
        }
    }

    public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback,
                                     FileChooserParams fileChooserParams) {
        this.filePathCallback = filePathCallback;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
            openFileExplorer();
        }else {
            requestStoragePermission();
        }
        return true;
    }
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this.activity, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            openFileExplorer();
            return;
        }

        ActivityCompat.requestPermissions(this.activity, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 123);
    }

    public void openFileExplorer() {
        mGetContent.launch("image/*");
    }
}
