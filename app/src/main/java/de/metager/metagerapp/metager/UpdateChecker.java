package de.metager.metagerapp.metager;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import de.metager.metagerapp.R;

public class UpdateChecker extends AsyncTask<Void, String, Boolean> {

    private Context mContext;
    private String currentVersion = null;

    public UpdateChecker(Context context) {
        mContext = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        try {
            PackageInfo pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            currentVersion = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        String releases = this.fetchReleases();

        if (releases == null) {
            return false;
        }

        String latestVersion = this.parseLatestVersion(releases);

        if (latestVersion == null || currentVersion == null) {
            return false;
        }

        if (!latestVersion.equals(currentVersion)) {
            return true;
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean updateAvailable) {
        super.onPostExecute(updateAvailable);
        if (!updateAvailable) {
            return;
        }

        // There is a update available. Lets install it
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage(R.string.update_dialog_message)
                .setTitle(R.string.update_dialog_title)
                .setCancelable(false);

        // Install Button
        builder.setPositiveButton(R.string.update_dialog_install, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Updater updater = new Updater(mContext);
                updater.execute();
            }
        });

        // Later Button
        builder.setNegativeButton(R.string.update_dialog_later, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @org.jetbrains.annotations.Nullable
    private String parseLatestVersion(String releases) {
        String latestVersion = null;
        try {
            JSONArray releasesJson = new JSONArray(releases);
            // Get the commit hash of the latest release
            String latestCommitHash = null;
            for (int i = 0; i < releasesJson.length(); i++) {
                JSONObject release = releasesJson.getJSONObject(i);
                String name = release.getString("name");
                if (name.equals("latest")) {
                    JSONObject commitData = release.getJSONObject("commit");
                    latestCommitHash = commitData.getString("id");
                    break;
                }
            }

            if (latestCommitHash == null) {
                return null;
            }

            // Now find a release with the same commit hash
            for (int i = 0; i < releasesJson.length(); i++) {
                JSONObject release = releasesJson.getJSONObject(i);
                String name = release.getString("name");
                if (!name.equals("latest")) {
                    JSONObject commitData = release.getJSONObject("commit");
                    String commitHash = commitData.getString("id");
                    if (commitHash.equals(latestCommitHash)) {
                        latestVersion = name;
                        break;
                    }
                }
            }
        } catch (JSONException e) {
            return null;
        }
        return latestVersion;
    }

    @org.jetbrains.annotations.Nullable
    private String fetchReleases() {
        String result = null;
        HttpURLConnection con = null;
        BufferedReader reader = null;
        try {
            String releasesUrl = "https://gitlab.metager.de/api/v4/projects/68/releases/";
            con = ((HttpURLConnection) new URL(releasesUrl).openConnection());
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuilder resultBuilder = new StringBuilder();
            String line = null;
            String newLine = System.getProperty("line.seperator");
            boolean flag = false;
            while ((line = reader.readLine()) != null) {
                resultBuilder.append(flag ? newLine : "").append(line);
                flag = true;
            }
            result = resultBuilder.toString();
        } catch (IOException e) {
            return null;
        } finally {
            if (con != null) {
                con.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
}
