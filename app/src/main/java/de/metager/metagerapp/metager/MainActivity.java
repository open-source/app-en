package de.metager.metagerapp.metager;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.webkit.JavaScriptReplyProxy;
import androidx.webkit.WebMessageCompat;
import androidx.webkit.WebViewCompat;
import androidx.webkit.WebViewFeature;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;

import de.metager.metagerapp.BuildConfig;
import de.metager.metagerapp.DownloadListener;
import de.metager.metagerapp.R;
import de.metager.metagerapp.tokens.TokenManager;

public class MainActivity extends AppCompatActivity {
    private WebView webview;
    private MGWebChromeClient webChromeClient;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        EdgeToEdge.enable(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.checkForUpdates();

        webview = findViewById(R.id.webview);

        this.enableBackGestures();
        webview.getSettings().setJavaScriptEnabled(true);


        if (BuildConfig.DEBUG) {
            WebView.setWebContentsDebuggingEnabled(true);
            webview.getSettings().setUserAgentString("MetaGer Development");
        }

        // Check current connectivity
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork && isConnectionFast(activeNetwork.getType(), activeNetwork.getSubtype())) {
            webview.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        } else {
            webview.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        }

        // Configure Cookie Management
        CookieManager.getInstance().setAcceptThirdPartyCookies(webview, false);

        webview.requestFocus(View.FOCUS_DOWN);

        // Check if user has a language preference set
        SharedPreferences prefs = this.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
        String lang = prefs.getString("lang", null);
        String locale = prefs.getString("locale", null);

        // MetaGer now supports traditional locales
        // We migrate the settings
        if (lang != null) {
            switch (lang) {
                case "german":
                case "spanish":
                    locale = "de-DE";
                    prefs.edit().putString("locale", locale).apply();
                    break;
                case "english":
                    locale = "en-US";
                    prefs.edit().putString("locale", locale).apply();
                    break;
            }
            prefs.edit().remove("lang").apply();
        }
        Map<String, String> supported_locales = new HashMap<>();
        supported_locales.put("de-DE", "de-DE");
        supported_locales.put("en-IE", "ie");
        supported_locales.put("en-UK", "uk");
        supported_locales.put("en-US", "en-US");
        if (locale == null) {
            Locale default_locale = Locale.getDefault();

            if (supported_locales.containsKey(default_locale.getLanguage() + "-" + default_locale.getCountry())) {
                // Locale is supported by MetaGer: Set it as default
                locale = supported_locales.get(default_locale.getLanguage() + "-" + default_locale.getCountry());
            } else if (default_locale.getLanguage().equals("de")) {
                // We do not support the default country: Default to Germany
                locale = "de-DE";
            } else if (default_locale.getLanguage().equals("en")) {
                // We do not support the default country: Default to US
                locale = "en-US";
            } else {
                // Global default is US
                locale = "en-US";
            }
            prefs.edit().putString("locale", locale).apply();
        }

        String url = getBaseUrl() + "/" + supported_locales.get(locale);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String intent_url_string = extras.getString("url");
            if (intent_url_string != null) {
                try {
                    URL intent_url = new URL(intent_url_string);

                } catch (MalformedURLException ignored) {
                }
            }
        }

        Preferences.getInstance().init(this);
        TokenManager.getInstance().init(url);

        DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        webview.setDownloadListener(new DownloadListener(downloadManager));
        MGWebviewClient webviewClient = new MGWebviewClient(prefs, supported_locales, webview, this, url);
        webview.setWebViewClient(webviewClient);
        this.webChromeClient = new MGWebChromeClient(findViewById(R.id.progressBar), this);
        webview.setWebChromeClient(this.webChromeClient);

        String query = this.getIntent().getStringExtra(SearchManager.QUERY);
        String startpage_string;
        if (query != null && !query.isEmpty()) {
            // WEB_SEARCH Intent was called. we'll open a result page
            try {
                url += "/meta/meta.ger3?eingabe=" + URLEncoder.encode(query, "utf8");
            } catch (UnsupportedEncodingException e) {
                Log.e("MetaGer App", "Unsupported charset \"utf8\"");
            }
        } else if ((startpage_string = getIntent().getStringExtra("startpage")) != null) {
            try {
                URL startpage_url = new URL(startpage_string);
                if (webviewClient.allowedHosts.contains(startpage_url.getHost())) {
                    url = startpage_url.toString();
                }
            } catch (MalformedURLException ignored) {
            }
        }

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.constraintLayout), (v, insets) -> {
            Insets bars = insets.getInsets(
                    WindowInsetsCompat.Type.systemBars()
                            | WindowInsetsCompat.Type.displayCutout()
            );
            v.setPadding(bars.left, bars.top, bars.right, bars.bottom);
            return WindowInsetsCompat.CONSUMED;
        });

        if (savedInstanceState == null) {
            // Override URL Loading is not called when manually calling loadUrl
            // So we manually call it here
            webviewClient.shouldOverrideUrlLoading(webview, url);
            webview.loadUrl(url);
        } else {
            webview.restoreState(savedInstanceState);
        }
    }

    private void checkForUpdates() {

        String appID = BuildConfig.APPLICATION_ID;

        //noinspection ConstantConditions
        if (!appID.equals("de.metager.metagerapp.manual")) {
            return;
        }

        //long updateCheckEvery = 2 * 60 * 60 * 1000;
        long updateCheckEvery = 60 * 1000;

        SharedPreferences prefs = this.getPreferences(Context.MODE_PRIVATE);
        long lastUpdateCheck = prefs.getLong("lastupdatecheck", 0);
        long now = System.currentTimeMillis();

        if ((now - lastUpdateCheck) > updateCheckEvery) {
            UpdateChecker updateChecker = new UpdateChecker(this);
            updateChecker.execute();
            SharedPreferences.Editor editor = prefs.edit();
            editor.putLong("lastupdatecheck", System.currentTimeMillis());
            editor.apply();
        }
    }

    private String getBaseUrl() {
        try {
            ActivityInfo ai = getPackageManager().getActivityInfo(getComponentName(), PackageManager.GET_META_DATA);
            Bundle metaData = ai.metaData;
            String baseUrl = metaData.getString("de.metager.metagerapp.BASE_URL");
            if (baseUrl != null && !baseUrl.isEmpty()) {
                return baseUrl;
            } else {
                return "https://metager.de";
            }
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        webview.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        webview.restoreState(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onConfigurationChanged(@androidx.annotation.NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private boolean isConnectionFast(int type, int subType) {
        if (type == ConnectivityManager.TYPE_WIFI) {
            return true;
        } else if (type == ConnectivityManager.TYPE_MOBILE) {
            switch (subType) {
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                    return false; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_CDMA:
                    return false; // ~ 14-64 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    return true; // ~ 400-1000 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    return true; // ~ 600-1400 kbps
                case TelephonyManager.NETWORK_TYPE_GPRS:
                    return false; // ~ 100 kbps
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                    return true; // ~ 2-14 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPA:
                    return true; // ~ 700-1700 kbps
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                    return true; // ~ 1-23 Mbps
                case TelephonyManager.NETWORK_TYPE_UMTS:
                    return true; // ~ 400-7000 kbps
                /*
                 * Above API level 7, make sure to set android:targetSdkVersion
                 * to appropriate level to use these
                 */
                case TelephonyManager.NETWORK_TYPE_EHRPD: // API level 11
                    return true; // ~ 1-2 Mbps
                case TelephonyManager.NETWORK_TYPE_EVDO_B: // API level 9
                    return true; // ~ 5 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPAP: // API level 13
                    return true; // ~ 10-20 Mbps
                case TelephonyManager.NETWORK_TYPE_IDEN: // API level 8
                    return false; // ~25 kbps
                case TelephonyManager.NETWORK_TYPE_LTE: // API level 11
                    return true; // ~ 10+ Mbps
                // Unknown
                case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                default:
                    return false;
            }
        } else {
            return false;
        }
    }

    private void initializeBackgroundColor() {
        SharedPreferences prefs = this.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);

        WebView webview = findViewById(R.id.webview);
        ConstraintLayout constaint_layout = findViewById(R.id.constraintLayout);

        int background_color = prefs.getInt("background_color", Color.rgb(255, 255, 255));
        webview.setBackgroundColor(background_color);
        constaint_layout.setBackgroundColor(background_color);

        SharedPreferences.OnSharedPreferenceChangeListener listener = (shared_preferences, key) -> {
            if (key == null || !key.equals("background_color")) return;
            int color = shared_preferences.getInt("background_color", Color.rgb(255, 255, 255));
            webview.setBackgroundColor(color);
            constaint_layout.setBackgroundColor(color);
        };
        prefs.registerOnSharedPreferenceChangeListener(listener);
    }

    public void enableBackGestures() {
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                if (webview.canGoBack()) {
                    webview.goBack();
                } else {
                    finish();
                    System.exit(0);
                }
            }
        };
        getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MGWebChromeClient.STORAGE_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                this.webChromeClient.openFileExplorer();
                //Displaying a toast
//                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, R.string.storage_denied, Toast.LENGTH_LONG).show();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void onNightModeChanged() {
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra("startpage", webview.getUrl());
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        startActivity(i);

    }
}
