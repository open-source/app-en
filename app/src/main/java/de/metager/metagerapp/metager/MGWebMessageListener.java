package de.metager.metagerapp.metager;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.webkit.JavaScriptReplyProxy;
import androidx.webkit.WebMessageCompat;
import androidx.webkit.WebViewCompat;
import androidx.webkit.WebViewFeature;

import com.google.common.util.concurrent.AsyncFunction;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import de.metager.metagerapp.tokens.MakePayment;
import de.metager.metagerapp.tokens.PaymentInformation;
import de.metager.metagerapp.tokens.Token;
import de.metager.metagerapp.tokens.TokenManager;
import de.metager.metagerapp.tokens.TokenRefill;

public class MGWebMessageListener implements WebViewCompat.WebMessageListener {

    @Override
    public void onPostMessage(@NonNull WebView view, @NonNull WebMessageCompat message, @NonNull Uri sourceOrigin, boolean isMainFrame, @NonNull JavaScriptReplyProxy replyProxy) {
        if (!WebViewFeature.isFeatureSupported(WebViewFeature.WEB_MESSAGE_LISTENER)) return;
        JsonObject json_message;
        JsonObject request;
        String requestType;
        String action;
        try {
            json_message = new Gson().fromJson(message.getData(), JsonObject.class);
            if (!json_message.get("sender").getAsString().equals("webpage")) return;
            request = json_message.get("payload").getAsJsonObject();
            requestType = request.get("type") == null ? "" : request.get("type").getAsString();
            action = request.get("action") == null ? "" : request.get("action").getAsString();
        } catch (JsonSyntaxException | NullPointerException e) {
            return;
        }
        json_message.addProperty("sender", "mgapp");
        if (requestType.equals("key") && action.equals("getcharge")) {
            json_message.add("payload", this.getKeyCharge());
            replyProxy.postMessage(json_message.toString());
        } else if (requestType.equals("tokenauthorization")) {
            switch (action) {
                case "pay": {
                    JsonObject payment_json = request.get("payload").getAsJsonObject();
                    PaymentInformation payment = PaymentInformation.FROM_JSON(payment_json);
                    Executor executor = Executors.newCachedThreadPool();
                    Futures.addCallback(this.makePayment(payment, executor), new FutureCallback<PaymentInformation>() {
                        @SuppressLint("RequiresFeature")
                        @Override
                        public void onSuccess(PaymentInformation result) {
                            json_message.add("payload", result.toJson());
                            replyProxy.postMessage(json_message.toString());
                        }

                        @SuppressLint("RequiresFeature")
                        @Override
                        public void onFailure(@NonNull Throwable t) {
                            JsonObject response = new JsonObject();
                            response.addProperty("status", "error");
                            response.addProperty("msg", t.getMessage());
                            replyProxy.postMessage(response.toString());
                        }
                    }, executor);
                    break;
                }
                case "gettoken": {
                    JsonObject payment_json = request.get("payment_request").getAsJsonObject();
                    PaymentInformation payment = PaymentInformation.FROM_JSON(payment_json);

                    Executor executor = Executors.newCachedThreadPool();
                    ListenableFuture<PaymentInformation> refillTask = Futures.submit(new TokenRefill(payment), executor);
                    Futures.addCallback(refillTask, new FutureCallback<PaymentInformation>() {
                        @SuppressLint("RequiresFeature")
                        @Override
                        public void onSuccess(PaymentInformation result) {
                            TokenManager tokenManager = TokenManager.getInstance();

                            while (!tokenManager.tokens.tokens.isEmpty() && payment.missing >= 1) {
                                payment.tokens.tokens.add(tokenManager.tokens.tokens.remove(0));
                                payment.missing = BigDecimal.valueOf(payment.missing - 1F).setScale(1, RoundingMode.HALF_UP).floatValue();
                            }

                            while (!tokenManager.tokens.decitokens.isEmpty() && payment.missing > 0) {
                                payment.tokens.decitokens.add(tokenManager.tokens.decitokens.remove(0));
                                payment.missing = BigDecimal.valueOf(payment.missing - 0.1F).setScale(1, RoundingMode.HALF_UP).floatValue();
                            }

                            while (!tokenManager.tokens.tokens.isEmpty() && payment.missing > 0) {
                                payment.tokens.tokens.add(tokenManager.tokens.tokens.remove(0));
                                payment.missing = BigDecimal.valueOf(payment.missing - 1F).setScale(1, RoundingMode.HALF_UP).floatValue();
                            }
                            json_message.add("payload", payment.toJson());
                            replyProxy.postMessage(json_message.toString());
                        }

                        @SuppressLint("RequiresFeature")
                        @Override
                        public void onFailure(@NonNull Throwable t) {
                            replyProxy.postMessage(json_message.toString());
                        }
                    }, executor);
                    break;
                }
                case "puttoken": {
                    JsonObject payment_json = request.get("payment_request").getAsJsonObject();
                    PaymentInformation payment = PaymentInformation.FROM_JSON(payment_json);
                    TokenManager tokenManager = TokenManager.getInstance();
                    for (Token token : payment.tokens.tokens) {
                        tokenManager.tokens.addToken(token);
                    }
                    for (Token token : payment.tokens.decitokens) {
                        tokenManager.tokens.addDecioken(token);
                    }
                    tokenManager.saveTokens();
                    payment.tokens.clear();
                    json_message.add("payload", payment.toJson());
                    replyProxy.postMessage(json_message.toString());
                    break;
                }
            }
        } else if (requestType.equals("settings_remove")) {
            String setting_key = request.get("setting_key") == null ? "" : request.get("setting_key").getAsString();
            JsonObject payload = new JsonObject();
            if (setting_key.equals("key")) {
                TokenManager.getInstance().removeKey();
                payload.addProperty("status", "ok");
            } else {
                // Setting not supported
                payload.addProperty("status", "error");
            }
            json_message.add("payload", payload);
            replyProxy.postMessage(json_message.toString());
        }
    }

    private ListenableFuture<PaymentInformation> makePayment(PaymentInformation payment, Executor executor) {
        JsonObject response = new JsonObject();

        TokenManager tokenManager = TokenManager.getInstance();
        response.addProperty("status", "ok");
        ListenableFuture<PaymentInformation> chargeTask = Futures.submit(new TokenRefill(payment), executor);
        return Futures.transformAsync(chargeTask, new AsyncFunction<PaymentInformation, PaymentInformation>() {
            @NonNull
            @Override
            public ListenableFuture<PaymentInformation> apply(PaymentInformation input) throws Exception {
                return Futures.submit(new MakePayment(payment), executor);
            }
        }, executor);
    }

    private JsonElement getKeyCharge() {
        JsonObject response = new JsonObject();
        response.addProperty("status", "ok");
        JsonObject data = new JsonObject();
        data.addProperty("charge", TokenManager.getInstance().getKeycharge());
        response.add("data", data);
        return response;
    }
}
