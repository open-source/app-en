package de.metager.metagerapp.metager;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;

import androidx.core.content.FileProvider;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Updater extends AsyncTask<Void, String, File> {

    private final Context mContext;

    public Updater(Context context) {
        this.mContext = context;
    }

    @Override
    protected File doInBackground(Void... voids) {
        File latestApk = this.downloadLatestApk();

        return latestApk;
    }

    @Override
    protected void onPostExecute(File latestApk) {
        super.onPostExecute(latestApk);
        if (latestApk == null) {
            return;
        }
        // Open Intent for installation
        Uri uri = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            uri = FileProvider.getUriForFile(mContext, mContext.getApplicationContext().getPackageName() + ".provider", latestApk);
        } else {
            uri = Uri.fromFile(latestApk);
        }

        Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
        intent.setDataAndType(uri, "application/vnd.android.package-archive");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true);
        mContext.startActivity(intent);

    }

    private File downloadLatestApk() {
        HttpURLConnection con = null;
        InputStream is = null;
        FileOutputStream fos = null;
        File latestApk = new File(mContext.getApplicationContext().getExternalCacheDir(), "metager-latest.apk");
        // File latestApk = new File(mContext.getExternalCacheDir(), "metager-latest.apk");
        try {
            String latestApkUrl = "https://gitlab.metager.de/open-source/app-en/-/raw/latest/app/release_manual/app-release_manual.apk";
            URL url = new URL(latestApkUrl);
            con = (HttpURLConnection) url.openConnection();
            is = con.getInputStream();
            fos = new FileOutputStream(latestApk, false);

            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                fos.write(buffer, 0, bytesRead);
            }
            fos.flush();
            return latestApk;
        } catch (MalformedURLException | FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                con.disconnect();
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
