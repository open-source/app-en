package de.metager.metagerapp.metager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.ArrayMap;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.webkit.JavaScriptReplyProxy;
import androidx.webkit.WebMessageCompat;
import androidx.webkit.WebViewCompat;
import androidx.webkit.WebViewFeature;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.metager.metagerapp.BuildConfig;
import de.metager.metagerapp.tokens.TokenManager;

public class MGWebviewClient extends WebViewClient {
    private final SharedPreferences prefs;
    private final Map<String, String> supported_locales;
    private final WebView webview;
    private final InputMethodManager inputManager;
    private final MainActivity mactivity;
    public final ArrayList<String> allowedHosts;

    /**
     * @noinspection ConstantValue, ConstantValue
     */
    public MGWebviewClient(SharedPreferences prefs, Map<String, String> supported_locales, WebView webview, MainActivity activity, String baseURL) {
        this.prefs = prefs;
        this.supported_locales = supported_locales;
        this.webview = webview;
        this.mactivity = activity;
        this.inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        CookieManager cookieManager = CookieManager.getInstance();
        allowedHosts = new ArrayList<>();
        allowedHosts.add("metager.de");
        allowedHosts.add("metager.org");
        allowedHosts.add("proxy.metager.de");
        try {
            URL url = new URL(baseURL);
            allowedHosts.add(url.getHost());
        } catch (MalformedURLException e) {
            Log.e("mgwebviewclient", e.toString());
        }
        //noinspection ConstantValue
        if (BuildConfig.BUILD_TYPE.equals("debug")) {
            this.allowedHosts.add("metager3.de");
        }

        if (WebViewFeature.isFeatureSupported(WebViewFeature.WEB_MESSAGE_LISTENER)) {
            ArrayList<String> origins = new ArrayList<String>() {{
                add("https://metager.de");
                add("https://metager.org");
                add("https://proxy.metager.de");
                //noinspection ConstantValue
                if (BuildConfig.BUILD_TYPE.equals("debug")) {
                    add("https://metager3.de");
                    try {
                        URI uri = new URI(baseURL);
                        String origin = uri.resolve("/").toString();
                        origin = origin.substring(0, origin.length() - 1);
                        add(origin);
                    } catch (URISyntaxException ignored) {
                    }
                }
            }};
            WebViewCompat.addWebMessageListener(webview, "mgapp", new HashSet<String>(origins), new MGWebMessageListener());
        }
    }

    @Override
    public void onPageFinished(WebView view, String url_string) {
        URL url = null;
        try {
            url = new URL(url_string);
        } catch (MalformedURLException ignored) {
        }

        if (url_string.matches(".*/meta/settings\\?.*")) {
            ThemeSwitcher.APPLY_STORED_STATE(this.mactivity);
        }

        if (url != null) {
            if (this.allowedHosts.contains(url.getHost())) {
                TokenManager.getInstance().consumeBrowserdata(url);
            }
            if (TokenManager.getInstance().isCookieManagementPath(url)) {
                TokenManager.getInstance().fetchKeyCharge();
            }
        }

        if (url_string.contains("/keys/key/enter")) {
            view.loadUrl("javascript:(function f() { document.getElementById('scan-qr-option').remove(); } )()");
        }
        view.loadUrl("javascript:(function f() { document.getElementById('plugin-btn').remove(); } )()");

        Pattern locale_pattern = Pattern.compile("(https?://)([^/]*)(/.*)?/lang(\\?.*|$)");
        Matcher locale_matcher = locale_pattern.matcher(url_string);
        if (locale_matcher.matches()) {
            String host = locale_matcher.group(2);
            String locale = locale_matcher.group(3);
            if (locale != null) {
                locale = locale.replaceFirst("^/", "");
            } else {
                if (host != null && host.equals("metager.de")) {
                    locale = "de-DE";
                } else if (host != null && host.equals("metager.org")) {
                    locale = "en-US";
                }
            }
            // Convert locale if shorted
            for (Map.Entry<String, String> entry : supported_locales.entrySet()) {
                if (entry.getValue().equals(locale)) {
                    locale = entry.getKey();
                }
            }
            // Check if current locale is the same as stored locale
            String stored_locale = prefs.getString("locale", null);

            if (stored_locale == null || !stored_locale.equals(locale)) {
                prefs.edit().putString("locale", locale).apply();
            }
        }

        // Show keyboard on the startpage
        if (url_string.matches("^https://metager\\.[a-z]{2,3}/(\\?.*)?$")) {
            inputManager.showSoftInput(webview, InputMethodManager.SHOW_IMPLICIT);
            webview.evaluateJavascript("document.querySelector('input[type=search]').focus();", null);
            webview.evaluateJavascript("document.getElementById('plugin-btn').outerHTML = '';", null);
        }
    }

    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
        if (!request.isForMainFrame()) return null;

        if (request.getMethod().equals("POST") && request.getUrl().getPath().matches("/meta/settings/.*")) {
            // Settings should be modified but attaching key as header does not currently work
            // for POST requests. This is why we need to attach the key as a cookie for this request
            CookieManager manager = CookieManager.getInstance();
            String cookie_value_string = "key=" + TokenManager.getInstance().getKey() + "; path=/meta/settings/; Max-Age=5";
            manager.setCookie(request.getUrl().getScheme() + "://" + request.getUrl().getHost(), cookie_value_string);
        }

        return null;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        String host = "";
        URL loadedURL = null;
        try {
            loadedURL = new URL(url);
            host = loadedURL.getHost();
        } catch (MalformedURLException e) {
            host = "";
        }

        if (this.allowedHosts.contains(host) && loadedURL != null) {
            if (!loadedURL.getHost().equals("proxy.metager.de")) {
                Map<String, String> tokensHeader = new ArrayMap<>();
                tokensHeader.put("mg-app", BuildConfig.VERSION_NAME);
                tokensHeader.putAll(TokenManager.getInstance().getHeaders(loadedURL));
                view.loadUrl(url, tokensHeader);
                if (tokensHeader.containsKey("anonymous-token-payment-id")) {
                    TokenManager.getInstance().syncPayment(tokensHeader.get("anonymous-token-payment-id"));
                }
            }
            return false;
        } else {
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            mactivity.startActivity(i);
            return true;
        }
    }
}
