package de.metager.metagerapp;

import android.app.DownloadManager;
import android.net.Uri;
import android.os.Environment;
import android.app.DownloadManager.Request;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DownloadListener implements android.webkit.DownloadListener {
    private final DownloadManager downloadManager;

    public DownloadListener(DownloadManager downloadManager) {
        this.downloadManager = downloadManager;
    }

    @Override
    public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
        String filename = "unknown_file";
        Pattern p = Pattern.compile("^attachment;.*filename=(.*?);?");
        Matcher m = p.matcher(contentDisposition);
        if(m.matches()){
            filename = m.group(1);
        }

        Request request = new Request(Uri.parse(url));
        request.setMimeType(mimetype);
        request.allowScanningByMediaScanner();
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, filename);
        request.setNotificationVisibility(Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

        downloadManager.enqueue(request);
    }
}
