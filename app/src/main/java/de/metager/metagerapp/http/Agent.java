package de.metager.metagerapp.http;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import de.metager.metagerapp.BuildConfig;

public class Agent {
    public static String USERAGENT() {
        return "MetaGerAppAndroid/" + BuildConfig.VERSION_NAME;
    }

    public static JsonObject PARSE_HTTP_RESPONSE(HttpURLConnection con) throws IOException {
        if (con.getResponseCode() >= 400) {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(con.getErrorStream()))) {
                return (new Gson()).fromJson(reader, JsonObject.class);
            }
        } else {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
                return (new Gson()).fromJson(reader, JsonObject.class);
            }
        }
    }
}
